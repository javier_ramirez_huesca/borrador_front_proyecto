import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial;*/ /*this avoid to apply parent styles to the component*/
          /*border: solid blue;*/
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
      <h2>Login Usuario</h2>
      <input type="text" placeholder="email" value="{{email::input}}" required="true"/>
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <button on-click="login">Login</button>
      <span hidden$="{{!isLogged}}">Bienvenido/a de nuevo ([[userID]])</span>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="manageAJAXResponse"
        on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      email: {
        type: String
      }, password: {
        type: String
      }, isLogged: {
        type: Boolean,
        value : false
      }, userID: {
        type: Number
      }
    };
  }

  login(){
    console.log("login button clicked");
    var loginData = {"email":this.email,"password":this.password};
    console.log(loginData);
    this.$.doLogin.body=JSON.stringify(loginData);
    this.$.doLogin.generateRequest();
    console.log("Request sent");

  }


  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.userID = data.detail.response.userID;
    this.isLogged=true;
    // emit login event
    this.dispatchEvent(
      new CustomEvent(
        "loginevent",
        {
          "detail":{ 
            "userID": this.userID
          }
        }
      )
    );
    console.log("Login event emmited for user "+this.userID);

  }

  showError(error){
    console.log("showError");
    console.log(error);
  }


}

window.customElements.define('login-usuario', LoginUsuario);
