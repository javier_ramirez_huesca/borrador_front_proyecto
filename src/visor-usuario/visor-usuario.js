import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../account-viewer/account-viewer.js';

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial;*/ /*this avoid to apply parent styles to the component*/
          /*border: solid blue;*/
        }

        .redbg{
          background-color: red;
        }

        .greenbg{
          background-color: green;
        }

        .bluebg{
          background-color: blue;
        }

        .greybg{
          background-color: grey;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h3>y mi email es [[email]]</h3>
      <button on-click="showAccounts">Ver Cuentas</button>
      <account-viewer id="accountViewer" userid=""></account-viewer>

      <iron-ajax
      id="getUser"
      url="http://localhost:3000/apitechu/v2/users/{{userID}}"
      handle-as="json"
      on-response="showUserData"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      }, last_name: {
        type: String
      }, email: {
        type: String
      }, id: {
        type: Number
      },
      "userID":{type:Number, observer:"_userIDchange"}
    };
  }

  showUserData(data){
    console.log("showUserData");
    console.log(data.detail.response);
    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;
  }

  _userIDchange(newValue, oldValue){
    console.log("UserID changed from "+oldValue+" to "+newValue);
    if(newValue!=null && newValue!=''){
      // execute back-end Request
      this.$.getUser.generateRequest();
    }
  }

  showAccounts(){
    console.log("showAccounts");
    this.$.accountViewer.userid=this.userID;
  }
}

window.customElements.define('visor-usuario', VisorUsuario);
