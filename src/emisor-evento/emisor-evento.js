import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class EmisorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial;*/ /*this avoid to apply parent styles to the component*/
          /*border: solid blue;*/
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
      <h3>emisor evento</h3>
      <button on-click="sendEvent">No Pulsar</button>

    `;
  }
  static get properties() {
    return {
    };
  }

  sendEvent(event){
    console.log("sendEvent");
    console.log(event);
    this.dispatchEvent(
      new CustomEvent(
        "myevent", // EVENT NAME:luego se utilizará en un nombre de att HTML, por eso lo dejamos en minúsculas
        {// objeto represent el emisor-evento
          "detail":{ // detail=payload evento
            "course": "TechU",
            "year": 2019
          }
        }
      )
    );

  }

}

window.customElements.define('emisor-evento', EmisorEvento);
