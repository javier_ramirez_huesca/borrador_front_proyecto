import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class UserLogout extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial;*/ /*this avoid to apply parent styles to the component*/
          /*border: solid blue;*/
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
      <a on-click="logout" href="">LogOut</a>
      <iron-ajax
        id="doLogout"
        url="http://localhost:3000/apitechu/v2/logout/{{userID}}"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="manageAJAXResponse"
        on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      userID: {type: Number}
    };
  }

  logout(){
    console.log("Loggin Out");
    this.$.doLogout.body='';
    this.$.doLogout.generateRequest();
    // emit logout event
    this.dispatchEvent(
      new CustomEvent(
        "logoutevent",
        {
          "detail":{
            "userID": this.userID
          }
        }
      )
    );
    console.log("Login event emmited for user "+this.userID);
  }


  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.userID = null;
    // emit login event
    this.dispatchEvent(
      new CustomEvent(
        "loginevent",
        {
          "detail":{
            "userID": this.userID
          }
        }
      )
    );
    console.log("Logout event emmited for user "+this.userID);

  }

  showError(error){
    console.log("showError");
    console.log(error);
  }


}

window.customElements.define('user-logout', UserLogout);
