import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';
import '../login-usuario/login-usuario.js';
import '../visor-usuario/visor-usuario.js';
import '../user-logout/user-logout.js';

/**
 * @customElement
 * @polymer
 */
class UserPanel extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <H1>Bienvenido</H1>
      <!-- <iron-pages selected="[[logged]]" attr-for-selected="logged">
        <div logged="false">
          <login-usuario on-loginevent="processLoginEvent"></login-usuario>
        </div>
        <div logged="true">
          <visor-usuario id="userViewer"></visor-usuario>
        </div>
        <div logged="false">
          <h2>LogOut</h2>
        </div>
      </iron-pages> -->
        <div hidden$="[[logged]]">
          <login-usuario on-loginevent="processLoginEvent"></login-usuario>
        </div>
        <div hidden$="[[!logged]]">
          <user-logout id="logout" on-logoutevent="processLogOutEvent"></user-logout>
        </div>
        <div hidden$="[[!logged]]">
          <visor-usuario id="userViewer"></visor-usuario>
        </div>
    `;
  }
  static get properties() {
    return {
      //"logged":{type:String,value:"false"}
      "logged":{type:Boolean,value:false}
    };
  }

  processLoginEvent(event){
    console.log("Login event received on User Panel for user "+event.detail.userID);
    // update userID property on user viewer
    this.$.userViewer.userID=event.detail.userID;
    this.$.logout.userID=event.detail.userID;
    //this.logged="true";
    this.logged=true;
  }

  processLogOutEvent(event){
    console.log("Logout event received on User Panel for user "+event.detail.userID);
    // update userID property on user viewer
    this.$.userViewer.userID=null;
    //this.logged="true";
    this.logged=false;
  }


}

window.customElements.define('user-panel', UserPanel);
