import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

/**
 * @customElement
 * @polymer
 */
class GestorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial;*/ /*this avoid to apply parent styles to the component*/
          /*border: solid blue;*/
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
      <h1>gestor evento</h1>
      <emisor-evento on-myevent="processEvent"></emisor-evento>
      <receptor-evento id="receiver"></receptor-evento>

    `;
  }
  static get properties() {
    return {
    };
  }

  processEvent(event){
    console.log("processEvent");
    console.log(event.detail);
    this.$.receiver.course = event.detail.course;
    this.$.receiver.year = event.detail.year;
  }

}

window.customElements.define('gestor-evento', GestorEvento);
