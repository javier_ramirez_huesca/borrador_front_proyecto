import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';

/**
 * @customElement
 * @polymer
 */
class AccountViewer extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial;*/ /*this avoid to apply parent styles to the component*/
          /*border: solid blue;*/
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
      <h2>Accounts of user [[userid]]</h2>
      <!--div class="row">
        <div class="col-3">IBAN</div>
        <div class="col-1">BALANCE</div>
      </div>
      <dom-repeat items="{{accounts}}">
        <template>
          <div class="row">
            <div class="col-3">[[item.IBAN]]</div>
            <div class="col-1">[[item.balance]]</div>
          </div>
        </template>
      </dom-repeat-->

      <vaadin-grid id="accountList" height-by-rows theme="row-stripes">
        <vaadin-grid-column path="IBAN" header="IBAN"></vaadin-grid-column>
        <vaadin-grid-column path="balance" header="BALANCE" text-align="end"></vaadin-grid-column>
      </vaadin-grid>


      <iron-ajax
        id="getAccounts"
        url="http://localhost:3000/apitechu/v2/accounts/{{userid}}"
        on-response="manageAJAXResponse"
        on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      /*IBAN: {
        type: String
      }, userID: {
        type: String
      }, balance: {
        type: Number
      }*/
      accounts:{
        type: Array
      }, userid:{
        type: String, observer:"_useridChanged"
      }
    };
  }


  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.accounts=data.detail.response;
    this.$.accountList.items = this.accounts;
    //document.querySelector('vaadin-grid').items = this.accounts;
  }

  showError(error){
    console.log("showError");
    console.log(error);
  }

  _useridChanged(newValue,oldValue){
    console.log("UserID changed from "+oldValue+" to "+newValue);
    if(newValue!=null && newValue!=''){
      console.log("Gather accounts info");
      this.$.getAccounts.generateRequest();
    }
  }

}

window.customElements.define('account-viewer', AccountViewer);
