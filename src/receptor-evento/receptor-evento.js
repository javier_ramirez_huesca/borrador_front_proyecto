import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class ReceptorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial;*/ /*this avoid to apply parent styles to the component*/
          /*border: solid blue;*/
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
      <h3>receptor evento</h3>
      <h4>Este curso es [[course]] del año [[year]]</h4>
      <input type="text" value="{{course::input}}"/>

    `;
  }
  static get properties() {
    return {
      "course":{type:String, observer: "_courseChanged"},
      "year":{type:String}
    };
  }

  _courseChanged(newValue, oldValue){
    console.log("Course changed:"+oldValue+" -> "+newValue);
  }

}

window.customElements.define('receptor-evento', ReceptorEvento);
